var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var cloudurl = "knot-test.cesar.org.br"
var cloudport = "3000";

var Users = require('../models/users');
var Thing = require('../models/thing');

const KNoTCloud = require('knot-cloud');


router.route('/')
    .get(async function (req, res, next) {

    const user = req.fulano.user
    const uuid = req.fulano.uuid;
    const token = req.fulano.token;
    const cloud = new KNoTCloud(cloudurl, cloudport, uuid, token);

    try {
        await cloud.connect();

        var devices = await cloud.getDevices();
        var gwId = devices[0].id;

        Thing.findOne({ id: gwId }, function (err, thing) {
            if (err) throw err;

            if (thing == null) {

            var mensagem = {"mensagem":"History not available"};
            res.json(mensagem);

            } else {

                res.json(thing.sensors);
            }
        });

        } catch (err) {
            console.log(err);
        }
        await cloud.close();
    })

    .post(async function (req, res, next) {

        const user = req.fulano.user
        const uuid = req.fulano.uuid;
        const token = req.fulano.token;

        const cloud = new KNoTCloud(cloudurl, cloudport, uuid, token);


        try {

            await cloud.connect();

            var devices = await cloud.getDevices();
            var gwId = devices[0].id;

            Thing.findOne({ id: gwId }, function (err, thing) {
            if (err) throw err;

            if (thing == null) {

            var mensagem = {"mensagem":"History not available"};
            res.json(mensagem);

            } else {

                console.log("Printing " + req.body.number + " most recent data");
                var thingLenght = thing.sensors.length;
                var recent = thingLenght - req.body.number;
                var i = 0;
                var j = 0;
                var response = [null];
                for (i=recent ; i < thingLenght ; i++) {
                    response[j]=thing.sensors[i];
                    j++;    
                 
                }
                res.json(response);
                
            }
        });

        } catch (err) {
            console.log(err);
        }

        await cloud.close();


    })

module.exports = router;
