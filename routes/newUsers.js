var express = require('express');
var router = express.Router();
var Users = require('../models/users');


router.route('/')

.post(function(req,res,next){
	Users.create(req.body,function(err,user) {
		if (err) throw err;
		console.log('User created!');
		var id = user._id;

		var mensagem = {"mensagem": "Added the user with id: " + id};
		res.json(mensagem);
	});
})

module.exports = router;
