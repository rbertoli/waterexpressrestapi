var express = require('express');
var router = express.Router();
var Users = require('../models/users');

router.route('/')
.get(function(req,res,next){
	Users.find({},function(err,users){
		if (err) throw err;
		res.json(users);
	});
})

.post(function(req,res,next){
	Users.create(req.body,function(err,user) {
		if (err) throw err;
		console.log('User created!');
		var id = user._id;

		res.writeHead(200, {
			'Content-Type' : 'text/plain'
		});
		res.end('Added the user with id: ' + id);
	});
})

module.exports = router;
