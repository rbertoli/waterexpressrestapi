var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
var cloudurl = "knot-test.cesar.org.br";
var cloudport = "3000";
var message = {"message": "Nivel baixo de agua"};
var emptymsg = {"message": ""};

const KNoTCloud = require('knot-cloud');


router.route('/')
	.post(async function (req, res, next) {

		const user = req.fulano.user
		const uuid = req.fulano.uuid;
		const token = req.fulano.token;

		const cloud = new KNoTCloud(cloudurl, cloudport, uuid, token);


		try {

			await cloud.connect();

			var devices = await cloud.getDevices();

			var gwId = devices[0].id;

			await cloud.setMetadata(gwId, req.body);

			var resp = await cloud.getDevice(gwId);

			var mensagem = {"mensagem":"Set threshold to " + resp.metadata.threshold }

			res.json(mensagem);

		} catch (err) {
			console.log(err);
		}

		await cloud.close();


	})
	.get(async function (req, res, next) {

		const user = req.fulano.user
		const uuid = req.fulano.uuid;
		const token = req.fulano.token;

		const cloud = new KNoTCloud(cloudurl, cloudport, uuid, token);


		try {

			await cloud.connect();

			var devices = await cloud.getDevices();

			var gwId = devices[0].id;


			var resp = await cloud.getDevice(gwId);
			var devicedata = await cloud.getData(gwId);


			var setpoint = parseInt(resp.metadata.threshold);
			var value = { "value":  devicedata[0].data.value };
			var threshold = { "threshold": setpoint };
			var response = [null];

			if (devicedata[0].data.value <= setpoint) {
				response[0] = value;
				response[1]	= threshold;
				response[2] = message;
				res.json(response);
			} else {
				response[0] = value;
				response[1]	= threshold;
				response[2] = emptymsg;
				res.json(response);	
			}


		} catch (err) {
			console.log(err);
		}

		await cloud.close();

	}


	);



module.exports = router;
