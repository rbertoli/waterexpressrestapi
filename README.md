## API REST WaterExpress


### Introdução

Permite monitorar o nível de um galão d’água utilizando a plataforma KNoT e manter um histórico de dados num banco de dados local.
Há uma rotina que verifica os dados de todos os devices por usuário e armazena em banco localmente a cada 30 segundos. 
Existem métodos que verificam o estado do garrafão em tempo real, permitindo a configuração de alertas de nível através da cloud KNoT. Para se elaborar relatórios de consumo e gráficos, existem métodos para obter os dados armazenados em banco de dados.


### Instalação

Instalar o MongoDB na máquina que irá abrigar o banco de dados.

Clone o repositório git e execute: 
``` shell
$ npm install
```
Realizar o devido apontamento no arquivo app.js na constante mongourl

Exemplo:
``` javascript
const mongourl = 'mongodb://localhost:27017/waterexpress';
```
Certifique-se que o MongoDB está rodando e execute:
``` shell
$ node app.js
```
### HTTP REST API

A maioria dos métodos requer autenticação, que é realizada através de autenticação básica via cabeçalho HTTP. 

Há uma rotina que atualiza os dados dos devices de cada usuário e persiste em banco a cada 30 segundos. 
Na primeira execução é exibida a seguinte mensagem na console: Thing <id da thing> history created.  
Nas demais execuções é exibido a mensagem:  Thing <id da thing> history updated.  


##### POST /newUser

Este método não requer autenticação e deve ser usado para criar o usuário para autenticar no sistema. 
Deve ser passado usuário, senha, UUID e Token do gateway do usuário via JSON no corpo da requisição.
Responde com o id da entrada no banco.

Exemplo de entrada:
```json
{ "user":"meuusuario",
  "password":"minhasenha",
  "uuid":"31813994-ba8c-4a0e-94c3-bcf7f37b0000",
  "token":"2dede7ea18be6737ab50b01b5b0d8d988165817f"
}
```
Exemplo de saída:
```json
{
    "mensagem": "Added the user with id: 5ba4f4d8a35c433111b29618"
}
```

OBS: Estes valores de UUID e TOKEN são do nosso device que ficará disponível para testes até 28/09/2018

##### GET /users

Lista a base de dados de usuário do sistema. 
Este método requer autenticação básica, passando usuário e senha no cabeçalho.

Exemplo:
``` json
[
    {
        "_id": "5ba05cda62547c385fdce1f4",
        "user": "admin",
        "password": "123QWEasd",
        "uuid": "31813994-ba8c-4a0e-94c3-bcf7f37b0000",
        "token": "2dede7ea18be6737ab50b01b5b0d8d988165817f",
        "thingId": [],
        "createdAt": "2018-09-18T02:03:06.218Z",
        "updatedAt": "2018-09-18T02:03:06.218Z",
        "__v": 0
    }
]
```

#### “Hot path”

##### GET /level

Mostra o nível da água e verifica se o nivel de agua está abaixo do threshold configurado. 
Em caso do nivel estar acima do threshold é retornado uma mensagem vazia.
Em caso do nivel estar abaixo do threshold é retornado uma mensagem de alerta.
Em ambos os casos é retornado um JSON com o nível da água, o threshold e a mensagem.
Este método requer autenticação básica, passando usuário e senha no cabeçalho.

Exemplos:

Nivel normal:
``` json
[
    {
        "value": 9
    },
    {
        "threshold": 4
    },
    {
        "message": ""
    }
]
```
Nivel baixo:
``` json
[
    {
        "value": 3
    },
    {
        "threshold": 4
    },
    {
        "message": "Nivel baixo de agua"
    }
]
```
##### POST /level

Configura o nível de água no qual a mensagem de alerta deve ser mostrada.
Deve ser feito um POST passando um JSON com o parametro threshold e seu valor.
Responde com o valor configurado na cloud KNoT para o device.
Este método requer autenticação básica, passando usuário e senha no cabeçalho.

Exemplo de entrada:
``` json
{ "threshold":"3"}
```
Resposta da requisição:
``` json
{
    "mensagem": "Set threshold to 3"
}
```

#### “Cold path”

##### GET /history

Verifica todo o histórico de dados armazenados em banco para o dispositivo.
Este método requer autenticação básica, passando usuário e senha no cabeçalho.

Exemplo:
``` json
[
    {
        "data": {
            "sensor_id": 2,
            "value": 6
        },
        "timestamp": "2018-09-18T13:45:46.937Z"
    },
    {
        "data": {
            "sensor_id": 2,
            "value": 5
        },
        "timestamp": "2018-09-18T13:46:16.959Z"
    },
    {
        "data": {
            "sensor_id": 2,
            "value": 4
        },
        "timestamp": "2018-09-18T13:46:47.010Z"
    },
    {
        "data": {
            "sensor_id": 2,
            "value": 3
        },
        "timestamp": "2018-09-18T13:47:17.018Z"
    },
    {
        "data": {
            "sensor_id": 2,
            "value": 2
        },
        "timestamp": "2018-09-18T13:47:47.096Z"
    },
    {
        "data": {
            "sensor_id": 2,
            "value": 1
        },
        "timestamp": "2018-09-18T13:48:17.070Z"
    },
    {
        "data": {
            "sensor_id": 2,
            "value": 0
        },
        "timestamp": "2018-09-18T13:48:47.159Z"
    }
]
```

##### POST /history 

 Obtém o histórico dos n últimos valores passando n via JSON através do parâmetro number. 
 Quando executada, a seguinte mensagem é exibida na console:
 Printing “n” most recent data.
Este método requer autenticação básica, passando usuário e senha no cabeçalho.

Exemplo de entrada:
``` json
{ "number":"3"}
```
Exemplo de resposta:
``` json
[
    {
        "data": {
            "sensor_id": 2,
            "value": 9
        },
        "timestamp": "2018-09-18T13:54:47.521Z"
    },
    {
        "data": {
            "sensor_id": 2,
            "value": 8
        },
        "timestamp": "2018-09-18T13:55:17.598Z"
    },
    {
        "data": {
            "sensor_id": 2,
            "value": 7
        },
        "timestamp": "2018-09-18T13:55:47.646Z"
    }
]
```

### Postman Collection

Foi disponibilizada uma collection do Postman para facilitar os testes desta API


