var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var newUsersRouter = require('./routes/newUsers');
var historyRouter = require('./routes/history');
var levelRouter = require('./routes/level');

var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var Users = require('./models/users');
var Thing = require('./models/thing');

const mongourl = 'mongodb://localhost:27017/waterexpress';
const KNoTCloud = require('knot-cloud');
var cloudurl = "knot-test.cesar.org.br"
var cloudport = "3000";


//função para autenticação básica
function auth(req, res, next) {
  var authHeader = req.headers.authorization;
  if (!authHeader) {
    var err = new Error('You are not authenticated!');
    err.status = 401;
    next(err);
    return;
  };



  var auth = new Buffer(authHeader.split(' ')[1], 'base64').toString().split(':');
  var user = auth[0];
  var pass = auth[1];

  // Busca usuário no banco de dados
  Users.findOne({ user: user }, function (err, usuario) {
    if (usuario == null) {
      var newerr = new Error('You are not authenticated!');
      newerr.status = 401;
      next(newerr);
    } else {
      if (usuario.password == pass) {
        req.fulano = usuario;
        next();

      } else {
        var newerr = new Error('You are not authenticated!');
        newerr.status = 401;
        next(newerr);
      }


    }


  });


};



// Conexão com o banco de dados
mongoose.connect(mongourl, {
  useNewUrlParser: true
  /* retira o warning de método depreciado */
});
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  console.log("Connected correctly to server");
});

var app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/newusers', newUsersRouter);

app.use(auth);

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/history', historyRouter);
app.use('/level', levelRouter);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(3000, function () {
  console.log('WaterExpress listening on port 3000!');
});

// Agendamento para coletar os dados dos devices e persistir em banco
setInterval(() => {
 Users.find({}, function (err, usr){
   if (err) throw err;

   usr.forEach(function(err,next) {

   if (usr[next].uuid != null) {

    saveData(usr[next])

   }

   })

  })
}, 30000);

// Função para pegar os dados dos devices dos usuarios e persistir em banco
async function saveData(req) {

    const user = req.user
    const uuid = req.uuid;
    const token = req.token;
    const cloud = new KNoTCloud(cloudurl, cloudport, uuid, token);

    try {
        await cloud.connect();

        var devices = await cloud.getDevices();
        var gwId = devices[0].id;
        var name = devices[0].name;
        var dataById = await cloud.getData(gwId);

        Thing.findOne({ id: gwId }, function (err, thing) {
            if (err) throw err;

            if (thing == null) {


                var myThing = new Thing({
                    uuid: uuid,
                    token: token,
                    name: name,
                    id: gwId,
                    sensors: [dataById[0]]
                })

                Thing.create(myThing, function (err, thing) {
                    if (err) throw err;
                    console.log("Thing " + thing.id + " history created.");

                });

            } else {

                thing.sensors.push(dataById[0]);
                thing.save(function (err, next) {
                    if (err) throw err;
                    console.log("Thing " + thing.id + " history updated.");
                });
            }

        })

    } catch (err) {
        console.log(err);
    }

    await cloud.close();
}


module.exports = app;
