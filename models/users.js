var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var sensorSchema = new Schema({
	sensorId: {type: Number },
    value: { type: Number, min: 0, max: 20}, 
},
	{ timestamps: true}
);


var thingSchema = new Schema({
	name: { type: String },
	id: { type: String },
	sensors: [sensorSchema]
},
	{timestamps: true}
);


var usuarioSchema = new Schema({
	user: { type: String, required: true },
	password: { type: String, required: true },
    uuid: { type: String, required: false },
	token: { type: String, required: false },
	threshold: { type: Number, required: false }, 
	thingId:[thingSchema]
},
	{timestamps: true}
);

var Users = mongoose.model('Usuario', usuarioSchema);
module.exports = Users;