var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var thingSchema = new Schema({
	uuid: { type: String, required: true  },
	token: { type: String, required: true },
	name: { type: String, required: true  },
	id: { type: String, required: true },
	sensors: [{}]
},
	{timestamps: true}
);


var Thing = mongoose.model('Thing', thingSchema);
module.exports = Thing;